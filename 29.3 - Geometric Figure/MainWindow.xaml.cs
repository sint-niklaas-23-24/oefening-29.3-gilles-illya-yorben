﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace _29._3___Geometric_Figure
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        int hoogte;
        int breedte;
        System.Windows.Shapes.Rectangle rect;
        System.Windows.Shapes.Polygon poly;
        PointCollection myPointCollection;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            myPointCollection = new PointCollection();
            myPointCollection.Add(new Point(0, 0));
            myPointCollection.Add(new Point(0, 1));
            myPointCollection.Add(new Point(1, 1));

        }

        private void btnVierkant_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rect = new System.Windows.Shapes.Rectangle();
                breedte = Convert.ToInt32(txtBreedte.Text);
                Vierkant vierkant = new Vierkant(Convert.ToInt32(breedte));
                rect.Width = vierkant.Zijde;
                rect.Height = vierkant.Zijde;
                rect.StrokeThickness = 2;
                rect.Stroke = new SolidColorBrush(Colors.Black);
                rect.Fill = new SolidColorBrush(Colors.BlueViolet);
                Canvas.SetLeft(rect, 50);
                Canvas.SetTop(rect, 150); //Dit is heel het window, niet het echte canvas, uit cnvdisplay aanspreken? > gaat niet
                cnvDisplay.Children.Add(rect);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Negatieve waarde", MessageBoxButton.OK);
            }
        }

        private void btnRechthoek_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                rect = new System.Windows.Shapes.Rectangle();
                breedte = Convert.ToInt32(txtBreedte.Text);
                hoogte = Convert.ToInt32(txtHoogte.Text);
                Rechthoek rechthoek = new Rechthoek(breedte, hoogte);
                rect.Width = rechthoek.Breedte;
                rect.Height = rechthoek.Hoogte;
                rect.StrokeThickness = 2;
                rect.Stroke = new SolidColorBrush(Colors.Black);
                rect.Fill = new SolidColorBrush(Colors.BlueViolet);
                Canvas.SetLeft(rect, 200);
                Canvas.SetTop(rect, 150); //Dit is heel het window, niet het echte canvas, uit cnvdisplay aanspreken? > gaat niet

                cnvDisplay.Children.Add(rect);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Negatieve waarde", MessageBoxButton.OK);
            }

        }

        private void btnDriehoek_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                poly = new System.Windows.Shapes.Polygon();
                breedte = Convert.ToInt32(txtBreedte.Text);
                hoogte = Convert.ToInt32(txtHoogte.Text);
                Driehoek driehoek = new Driehoek(breedte, hoogte);
                poly.Points = myPointCollection;
                poly.Width = driehoek.Breedte;
                poly.Height = driehoek.Hoogte;
                poly.StrokeThickness = 2;
                poly.Stroke = new SolidColorBrush(Colors.Black);
                poly.Fill = new SolidColorBrush(Colors.BlueViolet);
                poly.Stretch = Stretch.Fill;
                Canvas.SetLeft(poly, 550);
                Canvas.SetTop(poly, 150); //Dit is heel het window, niet het echte canvas, uit cnvdisplay aanspreken? > gaat niet

                cnvDisplay.Children.Add(poly);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Negatieve waarde", MessageBoxButton.OK);
            }

        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            cnvDisplay.Children.Clear();
        }
    }
}