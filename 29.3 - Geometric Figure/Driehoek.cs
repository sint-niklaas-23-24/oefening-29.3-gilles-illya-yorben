﻿namespace _29._3___Geometric_Figure
{
    internal class Driehoek : GeometricFigure
    {
        public Driehoek(int breedte, int hoogte) : base(breedte, hoogte)
        {

        }
        public override int Berekenoppervlakte() //Verplicht
        {
            return Hoogte * Breedte / 2;
        }

    }
}
