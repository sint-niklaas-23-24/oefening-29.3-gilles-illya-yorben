﻿namespace _29._3___Geometric_Figure
{
    internal class Vierkant : Rechthoek
    {
        private int _zijde;
        public Vierkant(int breedte, int hoogte) : base(breedte, hoogte)
        {
            if (breedte != hoogte)
            {
                breedte = hoogte;
            }
        }
        public Vierkant(int zijde)   //Foutmelding wanneer geen default in Superklasse&overerfende klasses (GeometricFigure > rechthoek)
        {
            Zijde = zijde;
        }
        public int Zijde
        {
            get { return _zijde; }
            set
            {
                if (value > 0)
                {
                    _zijde = value;
                }
                else
                {
                    throw new Exception("De zijde moet hoger zijn dan 0.");
                }
            }
        }
        public override int Berekenoppervlakte() //Verplicht
        {
            return base.Berekenoppervlakte();
        }
    }
}
