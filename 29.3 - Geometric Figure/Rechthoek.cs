﻿namespace _29._3___Geometric_Figure
{
    internal class Rechthoek : GeometricFigure
    {

        public Rechthoek(int breedte, int hoogte) : base(breedte, hoogte)
        {

        }
        public Rechthoek() : base() { }

        public override int Berekenoppervlakte() //Verplicht
        {
            return Hoogte * Breedte;
        }

    }
}
