﻿namespace _29._3___Geometric_Figure
{
    abstract class GeometricFigure
    {
        private int _hoogte;
        private int _breedte;
        public GeometricFigure(int breedte, int hoogte)
        {

            Breedte = breedte;
            Hoogte = hoogte;
        }
        public GeometricFigure() { }
        public int Hoogte
        {

            get { return _hoogte; }
            set
            {
                if (value > 0)
                {
                    _hoogte = value;
                }
                else
                {
                    throw new Exception("De hoogte moet hoger zijn dan 0.");
                }
            }
        }
        public int Breedte
        {
            get { return _breedte; }
            set
            {
                if (value > 0)
                {
                    _breedte = value;
                }
                else
                {
                    throw new Exception("De breedte moet hoger zijn dan 0.");
                }
            }
        }
        private int Oppervlakte
        {
            get { return Berekenoppervlakte(); }
        }

        public abstract int Berekenoppervlakte();
    }
}
